function counterFactory() {
    var counter = 0;
    var obj = {
        "increment" : function(){
            counter++;
            return counter;
        },
        "decrement" : function(){
            counter--;
            return counter;
        }
    };
    return obj;
}

module.exports = counterFactory;