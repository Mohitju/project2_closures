function cacheFunction(cb) {
    var obj={};
    function inner(){
        var str = JSON.stringify(arguments);
        if(obj[str] == undefined){
            console.log("function called ");
            obj[str]=cb(Object.values(arguments)[0]);
        }
        else{
            console.log("cached data returned");
        }
        return obj[str];
    }
    return inner;
}
module.exports = cacheFunction;