function limitFunctionCallCount(cb, n) {
    // Should return a function that invokes `cb`.
    // The returned function should only allow `cb` to be invoked `n` times.
    // Returning null is acceptable if cb can't be returned
      var counter = 0;
      function inner() {
          counter++;
          if(counter<=n){
              return cb;
          }
          else{
              return null;
          }
      }
      return inner;
  }
      
  module.exports = limitFunctionCallCount;